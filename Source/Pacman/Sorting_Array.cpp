#include "Sorting_Array.h"

void USorting_Array::Player_Scores_Sort(UPARAM(ref) TArray <FStructPlayerScore>& Array_To_Sort, const bool Descending, TArray <FStructPlayerScore>& Sorted_Array)
{
    Sorted_Array.Empty();
    Sorted_Array.Append(Array_To_Sort);
	
    Descending == true
    ? Sorted_Array.Sort([](const FStructPlayerScore& A, const FStructPlayerScore& B) { return A.Score > B.Score; })
    : Sorted_Array.Sort([](const FStructPlayerScore& A, const FStructPlayerScore& B) { return A.Score < B.Score; });
}

void USorting_Array::Player_Scores_Limit(UPARAM(ref) TArray<FStructPlayerScore>& Array_To_Limit, const int32 Max_Elements, TArray<FStructPlayerScore>& Limited_Array)
{
    Limited_Array.Empty();
    for (auto i = 0; i < Max_Elements && i < Array_To_Limit.Num(); i++)
        Limited_Array.Add(Array_To_Limit[i]);
}
