#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Sorting_Array.generated.h"

USTRUCT(BlueprintType)
struct FStructPlayerScore
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(BlueprintReadWrite)
	FName Name;

	UPROPERTY(BlueprintReadWrite)
	int32 Score;

	UPROPERTY(BlueprintReadWrite)
	FDateTime Time;
};

UCLASS()
class PACMAN_API USorting_Array final : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "Array Utils")
	static void Player_Scores_Sort(UPARAM(ref) TArray<FStructPlayerScore>& Array_To_Sort, bool Descending,
	                               TArray<FStructPlayerScore>& Sorted_Array);

	UFUNCTION(BlueprintCallable, Category = "Array Utils")
	static void Player_Scores_Limit(UPARAM(ref) TArray<FStructPlayerScore>& Array_To_Limit, int32 Max_Elements,
								   TArray<FStructPlayerScore>& Limited_Array);
};
